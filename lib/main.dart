import 'package:flutter/material.dart';
import 'splashScreen.dart';

/* void main() => runApp(ChangeNotifierProvider(
  create: (context) => AppManager(),
  child: MaterialApp(
    title: 'FMCG',
    debugShowCheckedModeBanner: false,
    theme: ThemeData(
        accentColor: Color(0XFF002C78),
        primaryColor: Color(0XFF002C78),
        scaffoldBackgroundColor: Color(0XFFFFFFFF)
        //primarySwatch: Colors.blue,
        ),
    home: SplashScreen(),
  ),
)); */
void main() => runApp(MaterialApp(
      title: 'Ihtmam',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          accentColor: Color(0XFF006DBF),
          primaryColor: Color(0XFF006DBF),
          scaffoldBackgroundColor: Color(0XFFFFFFFF)
          //primarySwatch: Colors.blue,
          ),
      home: SplashScreen(),
      builder: (context, child) {
        return Directionality(textDirection: TextDirection.rtl, child: child);
      },
    ));
