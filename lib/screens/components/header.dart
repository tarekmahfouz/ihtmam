import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(maxHeight: 250.0),
      child: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white60,
            ),
          ),
          ClipPath(
            clipper: HeaderClipper(),
            child: Container(
              height: 210.0,
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.grey[200],
              ),
            ),
          ),
          Positioned(
              top: 100.0,
              child: Container(
                child: Column(
                  children: [
                    Text(
                      'أهلا بك في',
                      style:
                          TextStyle(fontSize: 29.0, color: Color(0XFF006DBF)),
                    ),
                    Container(
                      height: 80.0,
                      width: MediaQuery.of(context).size.width / 2.0,
                      child: Image.asset(
                        'assets/images/logo.png',
                        fit: BoxFit.contain,
                      ),
                    ),
                  ],
                ),
              )),
        ],
      ),
    );
  }
}

class HeaderClipper extends CustomClipper<Path> {
  @override
  getClip(Size size) {
    var path = new Path();

    path.lineTo(0.0, size.height - 20);

    var firstControlPoint = new Offset(0.0, size.height);
    var firstEndPoint = new Offset(size.width * 0.2, size.height - 20);
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);

    var secondControlPoint = new Offset(size.width * 0.2, size.height - 20);
    var secondEndPoint = new Offset(size.width, size.height / 3);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy);

    path.lineTo(size.width, 0);

    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper oldClipper) {
    return true;
  }
}
