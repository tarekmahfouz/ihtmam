import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ihtmam/screens/components/header.dart';
import 'package:toast/toast.dart';

class IndividualLogin extends StatefulWidget {
  @override
  _IndividualLoginState createState() => _IndividualLoginState();
}

class _IndividualLoginState extends State<IndividualLogin> {
  final focus = FocusNode();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: _bodyBuilder(),
      ),
    );
  }

  Widget _bodyBuilder() {
    return Column(
      children: [
        _header(),
        Expanded(
          child: ListView(
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Padding(
                  padding: EdgeInsets.only(bottom: 30.0),
                  child: Text(
                    "ادخل اسمك وكلمة المرور",
                    style: TextStyle(fontSize: 22.0, color: Colors.black),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 30.0),
                child: TextField(
                  textInputAction: TextInputAction.next,
                  decoration: InputDecoration(
                      prefixIconConstraints:
                          BoxConstraints(minHeight: 14.0, minWidth: 14.0),
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0XFF006DBF), width: 1.0),
                        borderRadius: BorderRadius.circular(50.0),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0XFF006DBF), width: 1.0),
                        borderRadius: BorderRadius.circular(50.0),
                      ),
                      prefixIcon: Container(
                        margin: EdgeInsets.symmetric(horizontal: 10.0),
                        child: SvgPicture.asset(
                          'assets/icons/user-input-icon.svg',
                          color: Colors.black,
                          fit: BoxFit.contain,
                        ),
                      ),
                      hintText: 'اسم الموظف',
                      hintStyle: TextStyle()),
                  //autofocus: true,
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 30.0,
                ),
                child: TextField(
                  //scrollPadding: EdgeInsets.symmetric(horizontal: 20.0),
                  focusNode: focus,
                  style: TextStyle(),
                  obscureText: true,
                  decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0XFF006DBF), width: 1.0),
                        borderRadius: BorderRadius.circular(50.0),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0XFF006DBF), width: 1.0),
                        borderRadius: BorderRadius.circular(50.0),
                      ),
                      prefixIconConstraints:
                          BoxConstraints(minHeight: 14.0, minWidth: 14.0),
                      prefixIcon: Container(
                        margin: EdgeInsets.symmetric(horizontal: 10.0),
                        child: SvgPicture.asset(
                          'assets/icons/password-input-icon.svg',
                          color: Colors.black,
                          fit: BoxFit.contain,
                        ),
                      ),
                      suffix: GestureDetector(
                          child: Text("نسيت كلمة المرور؟",
                              style: TextStyle(
                                  //decoration: TextDecoration.underline,
                                  color: Colors.grey,
                                  fontSize: 10.0)),
                          onTap: () {
                            Toast.show('نسيت كلمة المرور', context,
                                duration: 1, gravity: Toast.BOTTOM);
                          }),
                      hintText: 'كلمة المرور',
                      hintStyle: TextStyle(
                        fontFamily: 'Cairo',
                        fontWeight: FontWeight.w600,
                      )),
                  //focusNode: AlwaysDisabledFocusNode(focus: focus),
                ),
              ),
              SizedBox(
                height: 30.0,
              ),
              _authButton(),
              SizedBox(
                height: 30.0,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Toast.show('نسيت كلمة المرور', context,
                            duration: 1, gravity: Toast.BOTTOM);
                      },
                      child: Text(
                        'نسيت كلمة المرور',
                        style: TextStyle(
                            color: Color(0XFF006DBF),
                            fontSize: 19.0,
                            decoration: TextDecoration.underline),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Toast.show('أنشئ حساب جديد', context,
                            duration: 1, gravity: Toast.BOTTOM);
                      },
                      child: Text(
                        'أنشئ حساب جديد',
                        style: TextStyle(
                            color: Color(0XFF006DBF),
                            fontSize: 19.0,
                            decoration: TextDecoration.underline),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        _footer()
      ],
    );
  }

  Widget _header() {
    return new Header();
  }

  Widget _footer() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        constraints: BoxConstraints(maxHeight: 125.0),
        width: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/footer.JPG'), fit: BoxFit.cover),
        ),
      ),
    );
  }

  Widget _authButton() {
    return FittedBox(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 103.0),
        child: GestureDetector(
          onTap: () {
            Toast.show('أهلا بك', context, duration: 1, gravity: Toast.BOTTOM);
          },
          child: Container(
            constraints: BoxConstraints(
              minHeight: 50.0,
            ),
            decoration: BoxDecoration(),
            child: Row(
              //mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Material(
                  color: Color(0XFFFABD00),
                  elevation: 0.0,
                  //clipBehavior: Clip.antiAlias,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                    topRight: Radius.circular(50.0),
                    bottomRight: Radius.circular(50.0),
                  )),
                  child: Container(
                    padding: EdgeInsets.all(10.0),
                    height: 50.0,
                    width: 40.0,
                    child: Icon(
                      Icons.login,
                      color: Color(0XFF006DBF),
                    ),
                  ),
                ),
                SizedBox(
                  width: 2.0,
                ),
                Material(
                  color: Theme.of(context).accentColor,
                  elevation: 0.0,
                  //clipBehavior: Clip.antiAlias,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(50.0),
                    bottomLeft: Radius.circular(50.0),
                  )),
                  child: Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
                    height: 50.0,
                    child: Text(
                      'تسجيل الدخول',
                      style: new TextStyle(fontSize: 22.0, color: Colors.white),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
