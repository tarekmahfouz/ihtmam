import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ihtmam/screens/login.dart';

import 'screens/home.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    loadData();
  }

  Future<Timer> loadData() async {
    return new Timer(Duration(seconds: 1), onDoneLoading);
  }

  onDoneLoading() async {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => IndividualLogin()));
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height,
          width: double.infinity,
          child: Image.asset(
            'assets/images/drawable-port-ldpi-screen.png',
            fit: BoxFit.cover,
          ),
          /* child: SvgPicture.asset(
            'assets/images/drawable-port-ldpi-screen.png',
            fit: BoxFit.cover,
          ), */
          decoration: BoxDecoration(),
        ),
        /* Container(
          margin: EdgeInsets.only(bottom: 10.0),
          alignment: Alignment.center,
          child: SvgPicture.asset(
            'assets/images/img_splashlogo.svg',
          ),
        ), */
        Container(
          margin: EdgeInsets.only(bottom: 10.0),
          alignment: Alignment.bottomCenter,
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
          ),
        ),
      ],
    );
  }
}
